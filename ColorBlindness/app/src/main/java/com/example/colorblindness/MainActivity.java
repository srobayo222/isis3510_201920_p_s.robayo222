package com.example.colorblindness;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.provider.MediaStore;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {


    ImageView ivImage;
    Integer REQUEST_CAMERA = 1, SELECT_FILE = 0;
    Spinner filters;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ivImage = findViewById(R.id.ivImage);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });

        filters = findViewById(R.id.filters);
        ArrayAdapter<CharSequence> adapter =  ArrayAdapter.createFromResource(this, R.array.filters, android.R.layout.simple_spinner_item );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        filters.setAdapter(adapter);
        filters.setOnItemSelectedListener(this);

        if(ivImage.getDrawable()==null) {
            filters.setEnabled(false);
        }

    }

    private void selectImage() {
        final CharSequence[] items = {"Camera","Gallery","Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Add Image");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                if(items[i].equals("Camera")){

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);

                } else if(items[i].equals("Gallery")) {

                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(intent.createChooser(intent,"Select File"), SELECT_FILE);

                } else if(items[i].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == Activity.RESULT_OK) {
            if(requestCode == REQUEST_CAMERA) {

                Bundle bundle = data.getExtras();
                final Bitmap bmp = (Bitmap) bundle.get("data");
                ivImage.setImageBitmap(bmp);
                filters.setEnabled(true);

            } else if(requestCode == SELECT_FILE){

                Uri selectedImageUri = data.getData();
                ivImage.setImageURI(selectedImageUri);
                filters.setEnabled(true);

            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String text = parent.getItemAtPosition(position).toString();
        if(text.equals("Normal")){
            float[] colorMatrix = {
                    1, 0, 0, 0, 0,
                    0, 1, 0, 0, 0,
                    0, 0, 1, 0, 0,
                    0, 0, 0, 1, 0
            };

            ColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);
            ivImage.setColorFilter(colorFilter);
        } else if (text.equals("Protanopia")){
            float[] colorMatrix = {
                    0.567f, 0.433f, 0, 0, 0,
                    0.558f, 0.442f, 0, 0, 0,
                    0, 0.242f, 0.758f, 0, 0,
                    0, 0, 0, 1, 0
            };

            ColorMatrix matrix = new ColorMatrix(colorMatrix);
            matrix.postConcat(new ColorMatrix(this.adjustHue()));

            ColorFilter colorFilter = new ColorMatrixColorFilter(matrix);
            ivImage.setColorFilter(colorFilter);

        } else if (text.equals("Protanomaly")){
            float[] colorMatrix = {
                    0.817f, 0.183f, 0, 0, 0,
                    0.333f, 0.667f, 0, 0, 0,
                    0, 0.125f, 0.875f, 0, 0,
                    0, 0, 0, 1, 0
            };
            ColorMatrix matrix = new ColorMatrix(colorMatrix);
            matrix.postConcat(new ColorMatrix(this.adjustHue()));

            ColorFilter colorFilter = new ColorMatrixColorFilter(matrix);
            ivImage.setColorFilter(colorFilter);
        } else if (text.equals("Deuteranopia")){
            float[] colorMatrix = {
                    0.625f, 0.375f , 0, 0, 0,
                    0.7f, 0.3f, 0, 0, 0,
                    0, 0.3f, 0.7f, 0, 0,
                    0, 0, 0, 1, 0
            };
            ColorMatrix matrix = new ColorMatrix(colorMatrix);
            matrix.postConcat(new ColorMatrix(this.adjustHue()));

            ColorFilter colorFilter = new ColorMatrixColorFilter(matrix);
            ivImage.setColorFilter(colorFilter);
        } else if (text.equals("Deuteranomaly")){
            float[] colorMatrix = {
                    0.8f, 0.2f, 0, 0, 0,
                    0.258f, 0.742f, 0, 0, 0,
                    0, 0.142f, 0.858f, 0, 0,
                    0, 0, 0, 1, 0
            };
            ColorMatrix matrix = new ColorMatrix(colorMatrix);
            matrix.postConcat(new ColorMatrix(this.adjustHue()));

            ColorFilter colorFilter = new ColorMatrixColorFilter(matrix);
            ivImage.setColorFilter(colorFilter);
        } else if (text.equals("Tritanopia")){
            float[] colorMatrix = {
                    0.95f, 0.05f, 0, 0, 0,
                    0, 0.433f, 0.567f, 0, 0,
                    0, 0.475f, 0.525f,0, 0,
                    0, 0, 0, 1, 0
            };
            ColorMatrix matrix = new ColorMatrix(colorMatrix);
            matrix.postConcat(new ColorMatrix(this.adjustHue()));

            ColorFilter colorFilter = new ColorMatrixColorFilter(matrix);
            ivImage.setColorFilter(colorFilter);
        } else if (text.equals("Tritanomaly")){
            float[] colorMatrix = {
                    0.967f, 0.033f, 0, 0, 0,
                    0 ,0.733f, 0.267f, 0, 0,
                    0 ,0.183f, 0.817f, 0, 0,
                    0 , 0, 0, 1, 0
            };
            ColorMatrix matrix = new ColorMatrix(colorMatrix);
            matrix.postConcat(new ColorMatrix(this.adjustHue()));

            ColorFilter colorFilter = new ColorMatrixColorFilter(matrix);
            ivImage.setColorFilter(colorFilter);
        } else if (text.equals("Achromatopsia")){
            float[] colorMatrix = {
                    0.299f, 0.587f, 0.114f, 0, 0,
                    0.299f, 0.587f, 0.114f, 0, 0,
                    0.299f, 0.587f, 0.114f, 0, 0,
                    0, 0, 0, 1, 0
            };

            ColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);
            ivImage.setColorFilter(colorFilter);
        } else if (text.equals("Achromatomaly")){
            float[] colorMatrix = {
                    0.618f, 0.320f, 0.062f, 0, 0,
                    0.163f, 0.775f, 0.062f, 0, 0,
                    0.163f, 0.320f, 0.516f, 0, 0,
                    0, 0, 0, 1, 0
            };

            ColorFilter colorFilter = new ColorMatrixColorFilter(colorMatrix);
            ivImage.setColorFilter(colorFilter);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public float[] adjustHue()
    {
        float value = 50f/ 180f * (float) Math.PI;
        if (value == 0)
        {
            return null;
        }
        float cosVal = (float) Math.cos(value);
        float sinVal = (float) Math.sin(value);
        float lumR = 0.213f;
        float lumG = 0.715f;
        float lumB = 0.072f;
        float[] mat = new float[]
                {
                        lumR + cosVal * (1 - lumR) + sinVal * (-lumR), lumG + cosVal * (-lumG) + sinVal * (-lumG), lumB + cosVal * (-lumB) + sinVal * (1 - lumB), 0, 0,
                        lumR + cosVal * (-lumR) + sinVal * (0.143f), lumG + cosVal * (1 - lumG) + sinVal * (0.140f), lumB + cosVal * (-lumB) + sinVal * (-0.283f), 0, 0,
                        lumR + cosVal * (-lumR) + sinVal * (-(1 - lumR)), lumG + cosVal * (-lumG) + sinVal * (lumG), lumB + cosVal * (1 - lumB) + sinVal * (lumB), 0, 0,
                        0f, 0f, 0f, 1f, 0f,
                        0f, 0f, 0f, 0f, 1f };
        return mat;
    }
}
